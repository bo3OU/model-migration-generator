<?php





class LexerArrayConfig implements LexerConfig
{
    /** @var TokenDefn[] */
    private $definitions = [];

    /**
     * @param TokenDefn[] $tokenDefinitions
     */
    public function __construct(array $tokenDefinitions)
    {
        foreach ($tokenDefinitions as $k => $v) {
            if ($v instanceof TokenDefn) {
                $this->addTokenDefinition($v);
            } elseif (is_string($k) && is_string($v)) {
                $this->addTokenDefinition(new TokenDefn($v, $k));
            }
        }
    }

    /**
     * @param TokenDefn $tokenDefn
     */
    public function addTokenDefinition(TokenDefn $tokenDefn)
    {
        $this->definitions[] = $tokenDefn;
    }

    public function getTokenDefinitions()
    {
        return $this->definitions;
    }
}

interface LexerConfig
{
    /**
     * @return TokenDefn[]
     */
    public function getTokenDefinitions();
}

class TokenDefn
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $regex;

    /**
     * @param string $name
     * @param string $regex
     * @param string $modifiers
     */
    public function __construct($name, $regex, $modifiers = 'i')
    {
        $this->name = $name;
        $delimiter = $this->findDelimiter($regex);
        $this->regex = sprintf('%s^%s%s%s', $delimiter, $regex, $delimiter, $modifiers);
        if (preg_match($this->regex, '') === false) {
            throw new \InvalidArgumentException(sprintf('Invalid regex for token %s : %s', $name, $regex));
        }
    }

    /**
     * @return string
     */
    public function getRegex()
    {
        return $this->regex;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $regex
     *
     * @return string
     */
    private function findDelimiter($regex)
    {
        static $choices = ['/', '|', '#', '~', '@'];
        foreach ($choices as $choice) {
            if (strpos($regex, $choice) === false) {
                return $choice;
            }
        }

        throw new \InvalidArgumentException(sprintf('Unable to determine delimiter for regex %s', $regex));
    }
}
class UnknownTokenException extends \RuntimeException
{
}

class Lexer
{
    /** @var LexerConfig */
    private $config;

    /** @var string */
    private $input;

    /** @var int */
    private $position;

    /** @var int */
    private $peek;

    /** @var Token[] */
    private $tokens;

    /** @var Token */
    private $lookahead;

    /** @var Token */
    private $token;

    /**
     * @param LexerConfig $config
     */
    public function __construct(LexerConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param LexerConfig $config
     * @param string      $input
     *
     * @return Token[]
     */
    public static function scan(LexerConfig $config, $input)
    {
        $tokens = [];
        $offset = 0;
        $position = 0;
        $matches = null;
        while (strlen($input)) {
            $anyMatch = false;
            foreach ($config->getTokenDefinitions() as $tokenDefinition) {
                if (preg_match($tokenDefinition->getRegex(), $input, $matches)) {
                    $str = $matches[0];
                    $len = strlen($str);
                    if (strlen($tokenDefinition->getName()) > 0) {
                        $tokens[] = new Token($tokenDefinition->getName(), $str, $offset, $position);
                        ++$position;
                    }
                    $input = substr($input, $len);
                    $anyMatch = true;
                    $offset += $len;
                    break;
                }
            }
            if (!$anyMatch) {
                throw new UnknownTokenException(sprintf('At offset %s: %s', $offset, substr($input, 0, 16).'...'));
            }
        }

        return $tokens;
    }

    /**
     * @return string
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return Token
     */
    public function getLookahead()
    {
        return $this->lookahead;
    }

    /**
     * @return Token
     */
    public function getToken()
    {
        return $this->token;
    }

    public function setInput($input)
    {
        $this->input = $input;
        $this->reset();
        $this->tokens = static::scan($this->config, $input);
    }

    public function reset()
    {
        $this->position = 0;
        $this->peek = 0;
        $this->token = null;
        $this->lookahead = null;
    }

    public function resetPeek()
    {
        $this->peek = 0;
    }

    public function resetPosition($position = 0)
    {
        $this->position = $position;
    }

    /**
     * @param string $tokenName
     *
     * @return bool
     */
    public function isNextToken($tokenName)
    {
        return null !== $this->lookahead && $this->lookahead->getName() === $tokenName;
    }

    /**
     * @param string[] $tokenNames
     *
     * @return bool
     */
    public function isNextTokenAny(array $tokenNames)
    {
        return null !== $this->lookahead && in_array($this->lookahead->getName(), $tokenNames, true);
    }

    /**
     * @return bool
     */
    public function moveNext()
    {
        $this->peek = 0;
        $this->token = $this->lookahead;
        $this->lookahead = (isset($this->tokens[$this->position]))
            ? $this->tokens[$this->position++]
            : null;

        return $this->lookahead !== null;
    }

    /**
     * @param string $tokenName
     */
    public function skipUntil($tokenName)
    {
        while ($this->lookahead !== null && $this->lookahead->getName() !== $tokenName) {
            $this->moveNext();
        }
    }

    /**
     * @param string[] $tokenNames
     */
    public function skipTokens(array $tokenNames)
    {
        while ($this->lookahead !== null && in_array($this->lookahead->getName(), $tokenNames, true)) {
            $this->moveNext();
        }
    }

    /**
     * Moves the lookahead token forward.
     *
     * @return null|Token
     */
    public function peek()
    {
        if (isset($this->tokens[$this->position + $this->peek])) {
            return $this->tokens[$this->position + $this->peek++];
        } else {
            return null;
        }
    }

    /**
     * @param string[] $tokenNames
     *
     * @return null|Token
     */
    public function peekWhileTokens(array $tokenNames)
    {
        while ($token = $this->peek()) {
            if (!in_array($token->getName(), $tokenNames, true)) {
                break;
            }
        }

        return $token;
    }

    /**
     * Peeks at the next token, returns it and immediately resets the peek.
     *
     * @return null|Token
     */
    public function glimpse()
    {
        $peek = $this->peek();
        $this->peek = 0;

        return $peek;
    }
}

class Token
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $value;

    /** @var int */
    protected $offset;

    /** @var int */
    protected $position;

    /**
     * @param string $name
     * @param string $value
     * @param string $offset
     * @param string $count
     */
    public function __construct($name, $value, $offset, $count)
    {
        $this->name = $name;
        $this->value = $value;
        $this->offset = $offset;
        $this->position = $count;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function is($token)
    {
        if ($token instanceof self) {
            return $this->name === $token->getName();
        } elseif (is_string($token)) {
            return $this->name === $token;
        } else {
            throw new \InvalidArgumentException('Expected string or Token');
        }
    }
}







$config = new LexerArrayConfig([
            ':'  => '',
            '->'  => 'ARROW',
            '\(' => 'OPENBRACKET',
            '\)' => 'CLOSEBRACKET',
            ','  => 'COMMA',
            '(has(one|many(|through))|belongstomany|morph(to|)many)' => 'RELATION'
            // regex for normal words except the regex up for relations !! 
            // regex for fieldtypes : integer, increments ...
        ]);

// static scan method that returns an array of
$tokens = Lexer::scan($config, 'hasmany()'); // input
array_map(function ($t) { return $t->getName(); }, $tokens);
 // ['number', 'plus', 'number']


// lexer instance
$lexer = new Lexer($config);
$lexer->setInput('hasmany()');// input
$lexer->moveNext();
while ($lexer->getLookahead()) {
    print $lexer->getLookahead()->getName() . " ";
    $lexer->moveNext();
}